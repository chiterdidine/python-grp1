FROM python:3.5.2
ADD index.html index.html
ADD server.py server.py
EXPOSE 8080
CMD  ["python3", "server.py"]
